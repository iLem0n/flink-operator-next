package ilem0n.de;

import org.eclipse.microprofile.config.inject.ConfigProperties;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Singleton;

@ApplicationScoped
public class FlinkSessionReconciler {
    private static final Logger log = LoggerFactory.getLogger(FlinkSessionReconciler.class);

    @ConfigProperty(name = "barfoo")
    String barfoo;

    FlinkSessionReconciler() {

    }

    public void testProps() {
        log.info(String.format("barfoo = %s", barfoo));
    }
}
