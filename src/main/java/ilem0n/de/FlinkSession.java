package ilem0n.de;

import io.fabric8.kubernetes.api.model.Namespaced;
import io.fabric8.kubernetes.client.CustomResource;
import io.fabric8.kubernetes.model.annotation.Group;
import io.fabric8.kubernetes.model.annotation.Kind;
import io.fabric8.kubernetes.model.annotation.Plural;
import io.fabric8.kubernetes.model.annotation.Version;

@Version("v1alpha1")
@Group("de.ilem0n")
@Kind("FlinkSession")
@Plural("flinksessions")
public class FlinkSession extends CustomResource<FlinkSessionSpec, FlinkSessionStatus> implements Namespaced {}

