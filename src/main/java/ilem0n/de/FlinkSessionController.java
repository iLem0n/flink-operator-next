package ilem0n.de;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.javaoperatorsdk.operator.api.*;
import io.javaoperatorsdk.operator.api.Context;
import io.javaoperatorsdk.operator.processing.event.EventSourceManager;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class FlinkSessionController implements ResourceController<FlinkSession> {
    private static final Logger log = LoggerFactory.getLogger(FlinkSessionController.class);

    private final KubernetesClient client;
    @ConfigProperty(name = "barfoo")
    String barfoo;

    public FlinkSessionController(KubernetesClient client) {
        this.client = client;
    }

    // TODO Fill in the rest of the controller

    @Override
    public void init(EventSourceManager eventSourceManager) {
        // TODO: fill in init
        log.info(String.format("BARFOO = %s", barfoo));
        new FlinkSessionReconciler().testProps();
    }

    @Override
    public UpdateControl<FlinkSession> createOrUpdateResource(
        FlinkSession resource, Context<FlinkSession> context) {
        // TODO: fill in logic

        return UpdateControl.noUpdate();
    }
}

